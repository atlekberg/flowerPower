# flowerPower
Automagically waters plants when you keep forgetting.

# Installation

```bash
sudo apt-get install python3-automationhat
```

# Documentation
## Pimoroni Automation HAT
* https://github.com/pimoroni/automation-hat
* https://learn.pimoroni.com/tutorial/sandyj/getting-started-with-automation-hat-and-phat
* https://github.com/pimoroni/automation-hat/blob/master/documentation/REFERENCE.md
* https://blog.pimoroni.com/automation-hat-tanya-teardown/
