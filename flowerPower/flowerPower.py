#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# flowerPower main Python app
# Copyright 2015 Atle Krogstad Berg (mail@atleberg.com)
#

import os
import sys
import re
import time
import datetime
import logging
import automationhat
import subprocess
import yaml
import argparse

__version__ = 0.2
__author__ = 'Atle Krogstad Berg - mail@atleberg.com'


## Functions
def checkWifiConnection():
	regex = r'(wlan\d)\s+(?:ESSID\:\")([\w\d\-]+)(?:\")'
	connected = False
	# # iwgetid
	# wlan0     ESSID:"PieInTheSky"
	try:
		result = subprocess.check_output(['/sbin/iwgetid'], shell=False)
	except:
		# automationhat.light.warn.on()
		pass
	else:
		# check return code
		command_output = result.decode('utf-8')
		if len(command_output.strip()) > 0:
			wlan = re.findall(regex, command_output, re.MULTILINE)
			for interface, currentWlan in wlan:
				logging.debug(currentWlan)
				if currentWlan == 'PieInTheSky':
					connected = True

	if connected:
		automationhat.light.comms.on()
		return True
	else:
		logging.error('Wifi not connected!')
		automationhat.light.comms.off()
		return False

def checkPower():
	if automationhat.input.one.read():
		automationhat.light.power.on()
		return True
	else:
		logging.error('Power not detected!')
		automationhat.light.power.off()
		return False


## APPLICATION ##

def main():
	parser = argparse.ArgumentParser(description='Flower Power Plant Management System')
	parser.add_argument('-d', '--debug', help='Turn on debug display', action='store_true')
	parser.add_argument('-w', '--when', help='When is this', required=True, choices=['morning', 'evening'])
	parser.add_argument('--dryrun', help='Don\'t actually do anything...', action='store_true')
	args = parser.parse_args()

	currentPath = os.path.dirname(os.path.abspath(sys.argv[0]))

	# Configure logging
	if not os.path.exists(os.path.join(currentPath, 'log')):
		os.mkdir(os.path.join(currentPath, 'log'))

	if args.debug:
		logging.basicConfig(level=logging.DEBUG)
	else:
		logging.basicConfig(level=logging.INFO)
	logger = logging.getLogger(os.path.basename(sys.argv[0]))
	handler = logging.FileHandler(os.path.join(os.path.join(currentPath, 'log'), 'flowerpower_%s.log' % datetime.date.today().strftime('%Y%m%d')))
	if args.debug:
		handler.setLevel(logging.DEBUG)
	else:
		handler.setLevel(logging.INFO)
	formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y.%m.%d %H:%M:%S')
	handler.setFormatter(formatter)
	logger.addHandler(handler)

	logger.info('Session started...')

	logger.info(f'Loading config file "{os.path.join(currentPath, "config.yml")}"')
	if os.path.exists(os.path.join(currentPath, 'config.yml')):
		with open(os.path.join(currentPath, 'config.yml'), 'r') as rf:
			fpConfig = yaml.safe_load(rf)
	else:
		logger.error('Could not find config file!')
		sys.exit(3)

	if args.dryrun:
		logger.info('Doing dry-run...')

	# Make sure all relays are off
	logger.debug('Set all relays to off position')
	automationhat.relay.one.off()
	automationhat.relay.two.off()
	automationhat.relay.three.off()

	# while True:
	wifiConn = checkWifiConnection()
	powerOk = checkPower()

	if powerOk and wifiConn:
		logger.debug('No warnings')
		automationhat.light.warn.off()
	else:
		automationhat.light.warn.on()

	if powerOk:
		# We are ok to use motors if neccessary
		for flower in fpConfig['flowers']:
			amount = flower[f'amount_{args.when}']
			if flower['enabled'] and amount > 0:
				logger.info(f'Watering {flower["name"]} with {amount} dl of water...')
				if not args.dryrun:
					logger.debug('Starting motor')
					automationhat.relay[flower['relay']].on()
				time.sleep(amount * flower['seconds_dl'])
				if not args.dryrun:
					logger.debug('Stopping motor')
					automationhat.relay[flower['relay']].off()
				logger.info(f'Completed for {flower["name"]}')
			else:
				logger.info(f'Skipping {flower["name"]} (disabled)')
	else:
		logger.error('No power available. Skipping run!')

	logger.info('Ending session')

if (__name__ == "__main__"):
	main()
